package compiler

import (
	"bitbucket.org/jatone/genieql/internal/errorsx"
)

// Well known errors for the compiler
const (
	ErrNoMatch = errorsx.String("no match")
)
